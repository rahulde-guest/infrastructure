#!/usr/bin/python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Upload the status of Go CD pipeline to a CI website.
"""

import json
import os
import time
import subprocess


def get_args():
    """Get the input arguments to the script from Go CD environment."""
    return {
        'server_url': os.environ['GO_SERVER_URL'],
        'fetch_user_file': os.environ['GO_FETCH_USER_FILE'],
        'pipeline_name': os.environ['GO_PIPELINE_NAME'],
        'upload_user': os.environ['ARTIFACT_UPLOAD_USER'],
        'upload_host': os.environ['ARTIFACT_HOST'],
        'upload_path': os.environ['ARTIFACT_UPLOAD_PATH']
    }


def get_pipeline_data(args):
    """Return the pipeline information."""
    user = None
    try:
        user = open(args['fetch_user_file']).read().strip()
    except Exception:
        print('Error reading fetch-user information file')

    user_args = []
    if user:
        user_args = ['--user', user]

    process = subprocess.run(
        ['curl', '-q', args['server_url'] + '/go/api/config/pipeline_groups'] +
        user_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
        check=True)
    return process.stdout.decode()


def get_result_data(args):
    """Return the result data for this pipeline."""
    result = 'Passed' if os.path.exists('passed') else 'Failed'
    data = {
        'pipeline_name': args['pipeline_name'],
        'result': result,
        'time': time.time()
    }
    return json.dumps(data)


def rsync(args, pipeline_info_file, status_file):
    """Copy the pipeline and result data to upload server."""
    target = '{}@{}:{}'.format(args['upload_user'], args['upload_host'],
                               args['upload_path'])
    subprocess.run(['rsync', pipeline_info_file, status_file, target],
                   check=True)


def main():
    """Upload the pipeline and status information to remote server."""
    args = get_args()

    pipeline_data = get_pipeline_data(args)
    pipeline_info_file = 'pipeline_info.json'
    open(pipeline_info_file, 'w').write(pipeline_data)

    result_data = get_result_data(args)
    status_file = args['pipeline_name'] + '_status.json'
    open(status_file, 'w').write(result_data)

    rsync(args, pipeline_info_file, status_file)


if __name__ == '__main__':
    main()
